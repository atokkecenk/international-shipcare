function changeType(type) {
    var satu = document.getElementById('date-1');
    var dua = document.getElementById('date-2');
    if (type == '1') {
        satu.style.display = 'block';
        dua.style.display = 'none';
    } else {
        dua.style.display = 'block';
        satu.style.display = 'none';
    }
}
// 
function showMessage(x) {
    // document.querySelector('.' + x).innerHTML = "Click to etail";
    document.querySelector('.' + x).style.color = "black";
}
function hideMessage(x) {
    // document.querySelector('.' + x).innerHTML = "";
    document.querySelector('.' + x).style.color = "transparent";
}

function detailHistoryCuti(register) {
    $.ajax({
        type: 'get',
        url: 'detail-history-cuti/' + register,
        success: function (res) {
            $('#modalDetailHistory').modal('show');
            $('#bodyDetailHistory').html(res);
        }
    });
}

$(document).ready(function () {
    $('.datatable').DataTable();

    $(".datepicker").datepicker({
        dateFormat: 'dd/mm/yy'
    });

    $(document).on('click', '#expModalCuti', function () {
        var id = $(this).data('id');
        $.ajax({
            type: 'get',
            url: 'detail-leave-by-id/' + id,
            success: function (res) {
                $('#bodyCuti').html(res);
            }
        });
    });

    $(document).on('click', '#expModalLevel', function () {
        var id = $(this).data('id');
        $.ajax({
            type: 'get',
            url: 'detail-level-by-id/' + id,
            success: function (res) {
                $('#bodyLevel').html(res);
            }
        });
    });

    $(document).on('click', '#expModalDelete', function () {
        var id = $(this).data('id');
        var mode = $(this).data('mode');
        $.ajax({
            type: 'get',
            url: 'delete-detail/' + mode + '/' + id,
            // dataType: 'html',
            success: function (res) {
                $('#bodyDelete').html(res);
            }
        });
    });

});