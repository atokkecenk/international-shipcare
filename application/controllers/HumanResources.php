<?php
defined('BASEPATH') or exit('No direct script access allowed');

class HumanResources extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->uid = $this->session->uid;
		$this->load->model('ModelHR', 'mHr');
		if (($this->session->userdata('authenticated') == 0)) {
			redirect('login');
		}
	}

	public function cuti()
	{
		$cek_cuti = $this->mHr->countSisaCuti($this->uid)->row();
		if ($cek_cuti->rows == 0) {
			$sisacuti = $this->db->get_where('_data_cuti', [
				'uid_employee' => $this->uid,
				'year' => date('Y'),
				'na' => 0
			]);
			if ($sisacuti->num_rows() == 0) {
				$sisacuti = 'Not Set';
			} else {
				$sisacuti = $sisacuti->row()->qty_cuti;
			}
		} else {
			$sisacuti = $cek_cuti->sisacuti;
		}

		$data['sisacuti'] = $sisacuti;
		$this->_renderpage('modul/human-resources/cuti', $data);
	}

	public function view_cuti()
	{
		if ($this->session->id_level == '3') {
			$mode = 'byUid';
		} else {
			$status =  isset($_GET['st']) ? $_GET['st'] : '';
			if ($status != '') {
				$mode = $status;
			} else {
				$mode = 'all';
			}
		}
		$data['mode'] = $mode;
		$data['data'] = $this->mHr->get_data_cuti($mode)->result();
		$this->_renderpage('modul/human-resources/cuti-view', $data);
	}

	public function save_cuti()
	{
		$type = $this->input->post('type');
		$date1 = $this->input->post('date1');
		$date_start = $this->input->post('date_start');
		$date_end = $this->input->post('date_end');
		$desc = $this->input->post('desc');

		if ($type == '>1') {
			if (empty($date_start) || empty($date_end)) {
				$this->session->set_flashdata('error', 'START/END DATE <b>REQUIRED !!</b>');
				redirect('leave', 'refresh');
			} else {
				if ($this->_changeDateFormat('-', $date_end) < $this->_changeDateFormat('-', $date_start)) {
					$this->session->set_flashdata('error', '<b>PLEASE CHECK</b> YOUR SELECTED DATE !!</>');
					redirect('leave', 'refresh');
				} else {
					$qty = $this->dateDiffInDays($this->_changeDateFormat('-', $date_start), $this->_changeDateFormat('-', $date_end));
				}
			}
		} else {
			if (empty($date1)) {
				$this->session->set_flashdata('error', '<b>REQUIRED</b> DATE !!');
				redirect('leave', 'refresh');
			} else {
				$qty = 1;
			}
		}

		$d_start = ($type == '1') ? $this->_changeDateFormat('/', $date1) : $this->_changeDateFormat('/', $date_start);
		$d_end = ($type == '1') ? null : $this->_changeDateFormat('/', $date_end);

		$register = $this->random('mix', 8);

		$ins = $this->db->insert('_data_cuti_transaksi', [
			'uid_employee' => $this->session->uid,
			'register' => $register,
			'type' => $type,
			'qty_cuti' => $qty,
			'start_date' => $d_start,
			'end_date' => $d_end,
			'year' => date('Y'),
			'description' => $desc,
			'user_create' => $this->session->uid,
			'create_date' => $this->_now()
		]);

		// To history
		$this->db->insert('_data_history', [
			'uid' => $this->session->uid,
			'register' => $register,
			'category' => '1',
			'note1' => $desc,
			'create_by' => $this->session->uid,
			'create_date' => $this->_now()
		]);

		if ($ins) {
			$this->session->set_flashdata('success', '<b>SUCCESSFUL</b> LEAVE APPLICATION');
			redirect('leave', 'refresh');
		} else {
			$this->session->set_flashdata('error', '<b>OOPS, FAILED</b> TO APPLY FOR LEAVE !!');
			redirect('leave', 'refresh');
		}
	}

	public function detail_cuti_byId($id)
	{
		$get = $this->mHr->get_data_cuti('byId', $id)->row();
		$slcA = $get->status == 'A' ? 'selected' : null;
		$slcR = $get->status == 'R' ? 'selected' : null;
		$note = !empty($get->description_apprjct) ? $get->description_apprjct : null;
		$html = '<p><b>Employee Name :</b> ' . $get->full_name . '<br><b>Date :</b> ' . $get->new_date . '</p><input type="hidden" name="id" value="' . $get->id . '"><div class="form-group">
				<label>Type</label>
				<select class="form-control" name="status" required>
					<option value="">- Select One -</option>		
					<option value="A" ' . $slcA . '>Approve</option>		
					<option value="R" ' . $slcR . '>Reject</option>		
				</select>
				</div>
				<div class="form-group">
				<label>Note</label>
				<textarea class="form-control" name="note" placeholder="Note" rows="3" required>' . $note . '</textarea>
				</div>';
		echo $html;
	}

	public function detail_history_cuti($register)
	{
		$get = $this->mHr->detail_history_cuti($register);
		$html = '<div class="vtl">';
		foreach ($get->result() as $val) {
			switch ($val->category) {
				case '2':
					$status = $val->history . ' to <b>' . $val->status . '</b><br>Note : ' . $val->note1 . '<br>Submit By : ' . $val->full_name;
					break;

				case '1':
					$status = $val->history . ' for ' . $val->qty_cuti . ' days (' . $val->leave_date . ')<br>Note : ' . $val->description . '<br>Submit By : ' . $val->full_name;
					break;
			}
			$html .= '<div class="event">
			 			<p class="date">' . $val->new_date . '</p>
			 			<p class="txt">' . $status . '</p>
			 		</div>';
		}
		$html .= '</div>';
		echo $html;
	}

	public function approve_cuti()
	{
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		$note = $this->input->post('note');

		$this->db->where('id', $id);
		$upd = $this->db->update('_data_cuti_transaksi', [
			'status' => $status,
			'description_apprjct' => $note,
			'update_by' => $this->session->uid,
			'update_date' => $this->_now()
		]);

		// To history
		$register = $this->db->get_where('_data_cuti_transaksi', ['id' => $id])->row()->register;
		$this->db->insert('_data_history', [
			'uid' => $this->session->uid,
			'register' => $register,
			'category' => '2',
			'var1' => $status,
			'note1' => $note,
			'create_by' => $this->session->uid,
			'create_date' => $this->_now()
		]);

		if ($upd) {
			$this->session->set_flashdata('success', '<b>SUCCESSFULLY</b> UPDATE LEAVE APPLICATION');
			redirect('leave-view', 'refresh');
		} else {
			$this->session->set_flashdata('error', '<b>OOPS, FAILED</b> UPDATE LEAVE APPLICATION !!');
			redirect('leave-view', 'refresh');
		}
	}

	function dateDiffInDays($date1, $date2)
	{
		// Calculating the difference in timestamps
		$diff = strtotime($date2) - strtotime($date1);
		// 1 day = 24 hours
		// 24 * 60 * 60 = 86400 seconds
		$diff = abs(round($diff / 86400));
		return (int) $diff + 1;
	}
}
