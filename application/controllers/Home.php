<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ModelHR', 'mHr');
		if (($this->session->userdata('authenticated') == 0)) {
			redirect('login');
		}
	}

	public function index()
	{
		$data['count_pending'] = $this->mHr->get_data_cuti('pending')->num_rows();
		$data['count_approve'] = $this->mHr->get_data_cuti('approve')->num_rows();
		$data['count_reject'] = $this->mHr->get_data_cuti('reject')->num_rows();
		$this->_renderpage('dashboard', $data);
	}

	public function view_level()
	{
		$data['data'] = $this->db->get_where('_sys_level', ['na' => 0])->result();
		$this->_renderpage('modul/master/level', $data);
	}

	public function detail_level_byId($id)
	{
		$get = $this->db->get_where('_sys_level', ['id' => $id])->row();
		$icCheck = $get->status == '1' ? 'checked' : '';
		$html = '<input type="hidden" name="id" value="' . $get->id . '">
				<div class="form-group">
				<label>Credential Name</label>
					<input type="text" class="form-control" name="level" value="' . $get->level_name . '">
				</div>
				<div class="form-group">
				<label>Alias</label>
					<input type="text" class="form-control" name="alias" value="' . $get->alias . '">
				</div>
				<div class="form-check">
                	<label class="form-check-label">
                    <input type="checkbox" class="form-check-input" name="status" ' . $icCheck . '>
                    Active
                    <i class="input-helper"></i></label>
					<small class="text-danger">Tick to active</small>
            	</div>';
		echo $html;
	}

	public function edit_save_level()
	{
		$id = $this->input->post('id');
		$level = $this->input->post('level');
		$alias = $this->input->post('alias');
		$status = $this->input->post('status');
		$status = isset($status) ? 1 : 0;

		$this->db->where('id', $id);
		$upd = $this->db->update('_sys_level', [
			'level_name' => $level,
			'alias' => $alias,
			'status' => (int) $status
		]);

		if ($upd) {
			$this->session->set_flashdata('success', '<b>SUCCESSFULLY</b> UPDATE CREDENTIAL');
			redirect('credential', 'refresh');
		} else {
			$this->session->set_flashdata('error', '<b>OOPS, FAILED</b> UPDATE CREDENTIAL !!');
			redirect('credential', 'refresh');
		}
	}
}
