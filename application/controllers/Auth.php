<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('authenticated') == 1)
            redirect('dashboard');

        $data['sett'] = $this->db->get_where('_sys_setting', ['id' => 1])->row();
        $this->load->view('login', $data);
    }

    public function login()
    {
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));

        $user = $this->db->get_where('_sys_user', ['username' => $username, 'na' => 0])->row();

        if (empty($user)) {
            $this->session->set_flashdata('error', 'USERNAME <b>NOT FOUND !!</b>');
            redirect('login');
        } else {
            if ($password == $user->password) {
                $get = $this->mMenu->data_login($username)->row();
                $session = array(
                    'authenticated' => 1,
                    'username' => $get->username,
                    'full_name' => $get->full_name,
                    'id_level' => $get->id_level,
                    'level' => $get->level_name,
                    'uid' => $get->uid
                );

                // $this->session->set_flashdata('success', 'Success');
                $this->session->set_userdata($session);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('error', '<b>WRONG</b> PASSWORD !!');
                redirect('login');
            }
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        // $this->session->authenticated = 0;
        redirect('login', 'refresh');
    }
}
