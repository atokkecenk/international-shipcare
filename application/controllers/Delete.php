<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Delete extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelHR', 'mHr');
    }

    public function del()
    {
        $mode = $this->input->post('mode');
        $id = $this->input->post('id');
        switch ($mode) {
            case 'cuti':
                $for = 'update';
                $table = '_data_cuti_transaksi';
                $field = 'id';
                $data = ['na' => 1];
                $redirect = 'leave-view';
                $message1 = 'DELETE DATA <b>SUCCESSFULLY</b>';
                $message2 = 'DELETE DATA <b>UNSUCCESSFULLY</b>';

                // To history
                $register = $this->db->get_where('_data_cuti_transaksi', ['id' => $id])->row()->register;
                $this->db->insert('_data_history', [
                    'uid' => $this->session->uid,
                    'register' => $register,
                    'category' => '3',
                    'create_by' => $this->session->uid,
                    'create_date' => $this->_now()
                ]);
                break;

            case 'level':
                $for = 'update';
                $table = '_sys_level';
                $field = 'id';
                $data = ['na' => 1];
                $redirect = 'credential';
                $message1 = 'DELETE DATA <b>SUCCESSFULLY</b>';
                $message2 = 'DELETE DATA <b>UNSUCCESSFULLY</b>';
                break;
        }

        $this->db->where($field, $id);
        if ($for == 'delete') {
            $act = $this->db->$for($table);
        } else {
            $act = $this->db->$for($table, $data);
        }

        if ($act) {
            $this->session->set_flashdata('success', $message1);
            redirect($redirect, 'refresh');
        } else {
            $this->session->set_flashdata('error', $message2);
            redirect($redirect, 'refresh');
        }
    }

    public function delete_detail($mode, $id)
    {
        switch ($mode) {
            case 'level':
            case 'cuti':
                $message = 'ARE YOU SURE DELETE THIS DATA ?';
                break;
        }
        // $get = $this->mHr->get_data_cuti('byId', $id)->row();
        $html = '<input type="hidden" name="mode" value="' . $mode . '"><input type="hidden" name="id" value="' . $id . '">' . $message;
        echo $html;
    }
}
