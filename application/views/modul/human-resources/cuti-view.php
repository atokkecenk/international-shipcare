<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body border-bottom">
            <div class="d-flex justify-content-between align-items-center flex-wrap">
                <h5 class="mb-2 mb-md-0 text-uppercase font-weight-medium">Leave View</h5>
                <!-- <button type="button" data-toggle="modal" data-target="#modalLevel" id="expModalLevel" class="btn btn-primary btn-sm btn-icon-text mr-3">
                    Add New
                    <i class="typcn typcn-plus btn-icon-append"></i>
                </button> -->
            </div>
        </div>
        <div class="card-body">
            <?php
            if ($this->session->flashdata('success')) {
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">' . $this->session->flashdata('success') . '
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>';
            } elseif ($this->session->flashdata('error')) {
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">' . $this->session->flashdata('error') . '
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>';
            }
            ?>
            <!-- <h4 class="card-title">Leave View</h4> -->
            <!-- <p class="card-description">
                Create Cuti
            </p> -->
            <div class="table-responsive">
                <table class="table table-striped datatable">
                    <thead>
                        <tr>
                            <th class="text-center">No.</th>
                            <th class="text-center">Employee Name</th>
                            <th class="text-center">Day</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Note</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Submit Date</th>
                            <!-- <th>Update Date</th> -->
                            <?php
                            if ($this->session->id_level != '3') {
                                echo '<th class="text-center">Action</th>';
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data as $dt) {
                            if ($dt->status == 'A') {
                                $sts = '<label class="badge badge-success pointer" onclick="detailHistoryCuti(\'' . $dt->register . '\')">' . $dt->new_status . '</label><br><small class="msg' . $no . '" style="font-size: 10px;color: transparent;">Click to detail</small>';
                            } elseif ($dt->status == 'R') {
                                $sts = '<label class="badge badge-danger pointer" onclick="detailHistoryCuti(\'' . $dt->register . '\')">' . $dt->new_status . '</label><br><small class="msg' . $no . '" style="font-size: 10px;color: transparent;">Click to detail</small>';
                            } else {
                                $sts = '<label class="badge badge-warning pointer" onclick="detailHistoryCuti(\'' . $dt->register . '\')">' . $dt->new_status . '</label><br><small class="msg' . $no . '" style="font-size: 10px;color: transparent;">Click to detail</small>';
                            }
                        ?>
                            <tr onmouseover="return showMessage('msg<?= $no ?>')" onmouseout="return hideMessage('msg<?= $no ?>')">
                                <td align="center"><?= $no++ ?>.</td>
                                <td><?= $dt->full_name ?></td>
                                <td align="center"><?= $dt->qty_cuti ?></td>
                                <td><?= $dt->new_date ?></td>
                                <td><?= $dt->description ?></td>
                                <td><?= $sts ?></td>
                                <td align="center"><?= $dt->submit_date ?></td>
                                <!-- <td align="center"><?= $dt->update_date ?></td> -->
                                <?php
                                if ($this->session->id_level != '3') {
                                ?>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <button type="button" data-toggle="modal" data-target="#modalCuti" id="expModalCuti" data-id="<?= $dt->id ?>" class="btn btn-primary btn-sm btn-icon-text mr-3">
                                                Edit
                                                <i class="typcn typcn-edit btn-icon-append"></i>
                                            </button>
                                            <button type="button" data-toggle="modal" data-target="#modalDelete" id="expModalDelete" data-id="<?= $dt->id ?>" data-mode="cuti" class="btn btn-danger btn-sm btn-icon-text mr-3">
                                                Delete
                                                <i class="typcn typcn-delete-outline btn-icon-append"></i>
                                            </button>
                                        </div>
                                    </td>
                                <?php
                                }
                                ?>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div id="testing"></div>
        </div>
    </div>
</div>

<!-- Modal Detail History -->
<div class="modal fade" id="modalDetailHistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Set Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
            <div class="modal-body">
                <div id="bodyDetailHistory"></div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalCuti" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Set Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('set-status-leave', 'method="post"') ?>
            <div class="modal-body">
                <div id="bodyCuti"></div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-icon-text mr-2"><i class="typcn typcn-document btn-icon-prepend"></i>Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>