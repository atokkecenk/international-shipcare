<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body border-bottom">
            <div class="d-flex justify-content-between align-items-center flex-wrap">
                <h5 class="mb-2 mb-md-0 text-uppercase font-weight-medium">Leave</h5>
            </div>
        </div>
        <div class="card-body">
            <?php
            if ($this->session->flashdata('success')) {
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">' . $this->session->flashdata('success') . '
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>';
            } elseif ($this->session->flashdata('error')) {
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">' . $this->session->flashdata('error') . '
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>';
            }
            ?>
            <!-- <h4 class="card-title">Leave</h4> -->
            <!-- <p class="card-description">
                Create Cuti
            </p> -->
            <?= form_open('save-leave', 'method="post" class="forms-sample"') ?>
            <h4 class="text-primary text-center">Remaining days off : <b><?= $sisacuti ?></b></h4>
            <div class="form-group">
                <label>Type</label>
                <select class="form-control" name="type" onchange="return changeType(this.value)" required>
                    <option value="">- Select One -</option>
                    <option value="1">One day</option>
                    <option value=">1">More than 1 days</option>
                </select>
            </div>
            <div class="form-group" id="date-1" style="display: none;">
                <label>Date</label>
                <div class="input-group">
                    <input type="text" name="date1" class="form-control datepicker" placeholder="dd/mm/yyyy">
                    <div class="input-group-append">
                        <button class="btn btn-sm btn-primary" type="button"><i class="typcn icon typcn-calendar"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group" id="date-2" style="display: none;">
                <label>Date <span class="text-danger">(Start - End)</span></label>
                <div class="row">
                    <div class="col-6 input-group">
                        <input type="text" name="date_start" class="form-control datepicker" placeholder="dd/mm/yyyy">
                        <div class="input-group-append">
                            <button class="btn btn-sm btn-primary" type="button"><i class="typcn icon typcn-calendar"></i></button>
                        </div>
                    </div>
                    <div class="col-6 input-group">
                        <input type="text" name="date_end" class="form-control datepicker" placeholder="dd/mm/yyyy">
                        <div class="input-group-append">
                            <button class="btn btn-sm btn-primary" type="button"><i class="typcn icon typcn-calendar"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Note</label>
                <textarea class="form-control" name="desc" placeholder="Note" rows="3" required></textarea>
            </div>
            <!-- <div class="form-group">
                    <label for="exampleInputEmail3">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail3" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword4">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword4" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="exampleSelectGender">Gender</label>
                    <select class="form-control" id="exampleSelectGender">
                        <option>Male</option>
                        <option>Female</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>File upload</label>
                    <input type="file" name="img[]" class="file-upload-default">
                    <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                        <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputCity1">City</label>
                    <input type="text" class="form-control" id="exampleInputCity1" placeholder="Location">
                </div>
                <div class="form-group">
                    <label for="exampleTextarea1">Textarea</label>
                    <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
                </div> -->
            <button type="submit" class="btn btn-primary btn-icon-text mr-2"><i class="typcn typcn-document btn-icon-prepend"></i>Save</button>
            <button type="button" class="btn btn-light">Cancel</button>
            <?= form_close() ?>
        </div>
    </div>
</div>