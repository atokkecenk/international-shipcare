<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body border-bottom">
            <div class="d-flex justify-content-between align-items-center flex-wrap">
                <h5 class="mb-2 mb-md-0 text-uppercase font-weight-medium">Credential View</h5>
                <button type="button" onclick="alert('Menu on next feature !!')" class="btn btn-primary btn-sm btn-icon-text mr-3">
                    Add New
                    <i class="typcn typcn-plus btn-icon-append"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <?php
            if ($this->session->flashdata('success')) {
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">' . $this->session->flashdata('success') . '
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>';
            } elseif ($this->session->flashdata('error')) {
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">' . $this->session->flashdata('error') . '
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>';
            }
            ?>
            <!-- <h4 class="card-title">Credential View</h4> -->
            <!-- <p class="card-description">
                Create Cuti
            </p> -->
            <div class="table-responsive">
                <table class="table table-striped datatable">
                    <thead>
                        <tr>
                            <th class="text-center">No.</th>
                            <th class="text-center">Credential Name</th>
                            <th class="text-center">Alias</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data as $dt) {
                            if ($dt->status == '1') {
                                $sts = '<label class="badge badge-success">Active</label>';
                            } else {
                                $sts = '<label class="badge badge-danger">Non-active</label>';
                            }
                        ?>
                            <tr>
                                <td align="center"><?= $no++ ?>.</td>
                                <td><?= $dt->level_name ?></td>
                                <td><?= $dt->alias ?></td>
                                <td align="center"><?= $sts ?></td>
                                <td align="center">
                                    <!-- <div class="d-flex align-items-center"> -->
                                    <button type="button" data-toggle="modal" data-target="#modalLevel" id="expModalLevel" data-id="<?= $dt->id ?>" class="btn btn-primary btn-sm btn-icon-text mr-3">
                                        Edit
                                        <i class="typcn typcn-edit btn-icon-append"></i>
                                    </button>
                                    <button type="button" data-toggle="modal" data-target="#modalDelete" id="expModalDelete" data-id="<?= $dt->id ?>" data-mode="level" class="btn btn-danger btn-sm btn-icon-text mr-3">
                                        Delete
                                        <i class="typcn typcn-delete-outline btn-icon-append"></i>
                                    </button>
                                    <!-- </div> -->
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalLevel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('save-edit-level', 'method="post"') ?>
            <div class="modal-body">
                <div id="bodyLevel"></div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-icon-text mr-2"><i class="typcn typcn-document btn-icon-prepend"></i>Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>