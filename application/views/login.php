<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $sett->name ?></title>
    <!-- base:css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/vendors/typicons/typicons.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/css/vertical-layout-light/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="<?= base_url() ?>assets/images/<?= $sett->favicon ?>" />
    <style>
        .to-center {
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth px-0">
                <div class="row w-100 mx-0">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                            <?php
                            if ($this->session->flashdata('success')) {
                                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">' . $this->session->flashdata('success') . '
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>';
                            } elseif ($this->session->flashdata('error')) {
                                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">' . $this->session->flashdata('error') . '
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>';
                            }
                            ?>

                            <div class="to-center" style="margin-bottom: 1rem;">
                                <img src="<?= base_url('assets/images/' . $sett->login_logo) ?>" alt="logo">
                            </div>
                            <h4 class="to-center" style="margin-bottom: 2rem;"><?= $sett->login_name ?></h4>
                            <h6 class="font-weight-light">Sign in to continue.</h6>
                            <!-- <form class="pt-3"> -->
                            <?= form_open('cek-login', 'method="post" class="pt-3"') ?>
                            <div class="form-group">
                                <input type="text" name="username" class="form-control form-control-lg" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control form-control-lg" placeholder="Password" required>
                            </div>
                            <div class="mt-3">
                                <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">SIGN IN</button>
                            </div>
                            <div class="my-2 d-flex justify-content-between align-items-center">
                                <!-- <div class="form-check">
                                        <label class="form-check-label text-muted">
                                            <input type="checkbox" class="form-check-input">
                                            Keep me signed in
                                        </label>
                                    </div> -->
                                <a href="#" class="auth-link text-black">Forgot password?</a>
                            </div>
                            <!-- <div class="mb-2">
                                    <button type="button" class="btn btn-block btn-facebook auth-form-btn">
                                        <i class="typcn typcn-social-facebook mr-2"></i>Connect using facebook
                                    </button>
                                </div>
                                <div class="text-center mt-4 font-weight-light">
                                    Don't have an account? <a href="register.html" class="text-primary">Create</a>
                                </div> -->
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- base:js -->
    <script src="<?= base_url() ?>assets/template/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="<?= base_url() ?>assets/template/js/off-canvas.js"></script>
    <script src="<?= base_url() ?>assets/template/js/hoverable-collapse.js"></script>
    <script src="<?= base_url() ?>assets/template/js/template.js"></script>
    <script src="<?= base_url() ?>assets/template/js/settings.js"></script>
    <script src="<?= base_url() ?>assets/template/js/todolist.js"></script>
    <!-- endinject -->
</body>

</html>