</div>
<!-- content-wrapper ends -->
<!-- partial:partials/_footer.html -->
<footer class="footer">
    <div class="card">
        <div class="card-body">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2022 <a href="#" class="text-muted" target="_blank"><?= $sett->name ?></a>. All rights reserved.</span>
                <!-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center text-muted">Free <a href="https://www.bootstrapdash.com/" class="text-muted" target="_blank">Bootstrap dashboard</a> templates from Bootstrapdash.com</span> -->
            </div>
        </div>
    </div>
</footer>
<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- Modal -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('delete', 'method="post"') ?>
            <div class="modal-body">
                <div id="bodyDelete" class="text-center"></div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-icon-text mr-2"><i class="typcn typcn-document btn-icon-prepend"></i>Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>
<!--  -->
<!-- base:js -->
<!-- Added -->
<script src="<?= base_url() ?>assets/template/jquery/jquery-3.6.0.js"></script>
<script src="<?= base_url() ?>assets/template/vendors/js/vendor.bundle.base.js"></script>
<script src="<?= base_url() ?>assets/template/jquery/jquery-ui.js"></script>
<script src="<?= base_url() ?>assets/template/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/template/datatables/dataTables.bootstrap4.min.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="<?= base_url() ?>assets/template/vendors/chart.js/Chart.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="<?= base_url() ?>assets/template/js/off-canvas.js"></script>
<script src="<?= base_url() ?>assets/template/js/hoverable-collapse.js"></script>
<script src="<?= base_url() ?>assets/template/js/template.js"></script>
<script src="<?= base_url() ?>assets/template/js/settings.js"></script>
<script src="<?= base_url() ?>assets/template/js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="<?= base_url() ?>assets/template/js/dashboard.js"></script>
<!-- End custom js for this page-->
<!-- My Script -->
<script src="<?= base_url() ?>assets/myscript/myScript.js"></script>
</body>

</html>