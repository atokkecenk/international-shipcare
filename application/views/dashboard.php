<div class="row">
	<!-- <div class="col-xl-6 grid-margin stretch-card flex-column">
		<h5 class="mb-2 text-titlecase mb-4">Status statistics</h5>
		<div class="row h-100">
			<div class="col-md-6 stretch-card">
				<div class="card">
					<div class="card-header">Leave Application</div>
					<div class="card-body">
						<div class="row h-100">
							<div class="col-4 d-flex flex-column justify-content-between">
								<p class="text-muted">Pending</p>
								<h4>55</h4>
							</div>
							<div class="col-4 d-flex flex-column justify-content-between">
								<p class="text-muted">Approve</p>
								<h4>123</h4>
							</div>
							<div class="col-4 d-flex flex-column justify-content-between">
								<p class="text-muted">Reject</p>
								<h4>123</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->
	<div class="col-md-4">
		<div class="card">
			<!-- <div class="card-header">Leave Application</div> -->
			<div class="card-body border-bottom">
				<div class="d-flex justify-content-between align-items-center flex-wrap">
					<h6 class="mb-2 mb-md-0 text-uppercase font-weight-medium">Leave Application</h6>
				</div>
			</div>
			<div class="card-body">
				<div class="row h-100">
					<div class="col-4 d-flex flex-column justify-content-between">
						<p class="text-muted">PENDING</p>
						<h4><?= $count_pending ?></h4>
						<a href="leave-view?st=pending" class="text-detail">See Details<i class="typcn icon typcn-arrow-right active-before transition active"></i></a>
					</div>
					<div class="col-4 d-flex flex-column justify-content-between">
						<p class="text-muted">APPROVE</p>
						<h4><?= $count_approve ?></h4>
						<a href="leave-view?st=approve" class="text-detail">See Details<i class="typcn icon typcn-arrow-right active-before transition active"></i></a>
					</div>
					<div class="col-4 d-flex flex-column justify-content-between">
						<p class="text-muted">REJECT</p>
						<h4><?= $count_reject ?></h4>
						<a href="leave-view?st=reject" class="text-detail">See Details<i class="typcn icon typcn-arrow-right active-before transition active"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>