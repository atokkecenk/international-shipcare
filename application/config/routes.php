<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'Auth';
$route['dashboard'] = 'Home';

// Auth
$route['login'] = 'Auth';
$route['logout'] = 'Auth/logout';
$route['cek-login'] = 'Auth/login';

// HR
$route['leave'] = 'HumanResources/cuti';
$route['leave-view'] = 'HumanResources/view_cuti';
$route['save-leave'] = 'HumanResources/save_cuti';
$route['set-status-leave'] = 'HumanResources/approve_cuti';
$route['detail-leave-by-id/(.+)'] = 'HumanResources/detail_cuti_byId/$1';
$route['detail-history-cuti/(.+)'] = 'HumanResources/detail_history_cuti/$1';

// MASTER
$route['credential'] = 'Home/view_level';
$route['detail-level-by-id/(.+)'] = 'Home/detail_level_byId/$1';
$route['save-edit-level'] = 'Home/edit_save_level';

// Delete
$route['delete'] = 'Delete/del';
$route['delete-detail/(.+)'] = 'Delete/delete_detail/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
