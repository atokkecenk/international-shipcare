<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->model('ModelMain', 'mMain');
        // if (!$this->session->userdata('authenticated') && !$this->session->userdata('muafakat_technology'))
        // redirect('auth');
    }

    function _renderpage($content, $data = NULL)
    {
        $uri1 = empty($this->uri->segment(1)) ? 'dashboard' : $this->uri->segment(1);
        $data['navtext'] = $this->mMenu->get_menu_by_route($uri1);
        $data['sett'] = $this->db->get_where('_sys_setting', ['id' => 1])->row();
        $data['header'] = $this->load->view('template/header', $data, TRUE);
        $data['content'] = $this->load->view($content, $data, TRUE);
        $data['footer'] = $this->load->view('template/footer', $data, TRUE);
        $this->load->view('index', $data);
    }

    function _now()
    {
        return date('Y-m-d H:i:s');
    }

    function _changeDateFormat($format, $date)
    {
        if ($format == '/') {
            $date = explode('/', $date);
            $date = $date[2] . '-' . $date[1] . '-' . $date[0];
        } elseif ($format == '-') {
            $date = str_replace('/', '-', $date);
        }
        return $date;
    }

    function random($mode, $length)
    {
        $value = '';
        switch ($mode) {
            case 'string-low':
                $chars = 'abcdefghijklmnopqrstuvwxyz';
                break;
            case 'string-upper':
                $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'number':
                $chars = '1234567890';
                break;
            case 'mix':
                $chars = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
        }

        for ($i = 0; $i < $length; $i++) {
            $rand = rand(0, strlen($chars) - 1);
            $value .= substr($chars, $rand, 1);
        }

        return $value;
    }
}
