<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelHR extends CI_Model
{
    public function countSisaCuti($uid)
    {
        return $this->db->query("SELECT
                                    COUNT(*) rows,
                                    dc.uid_employee,
                                    dc.qty_cuti qty_cuti,
                                    dc.qty_cuti - (
                                    SUM( ct.qty_cuti )) sisacuti 
                                FROM
                                    `_data_cuti` dc
                                    LEFT JOIN _data_cuti_transaksi ct ON dc.uid_employee = ct.uid_employee 
                                WHERE
                                    dc.uid_employee = '$uid'
                                    AND dc.na = '0' 
                                    AND ct.`status` = 'A'
                                    AND ct.na = '0'
                                ");
    }

    public function get_data_cuti($format, $id = null)
    {
        $id_level = $this->session->id_level;
        $uid = $this->session->uid;
        if ($format == 'all') {
            $where = "ct.na = '0'";
        } elseif ($format == 'byId') {
            $where = "ct.id = '$id'";
        } elseif ($format == 'byUid') {
            $status = isset($_GET['st']) ? $_GET['st'] : '';
            if ($status == '') {
                $where = "ct.na = '0' and ct.uid_employee = '$uid'";
            } else {
                switch ($status) {
                    case 'pending':
                        $addWhere = "ct.status is null";
                        break;
                    case 'approve':
                        $addWhere = "ct.status = 'A'";
                        break;
                    case 'reject':
                        $addWhere = "ct.status = 'R'";
                        break;
                }
                $where = "ct.na = '0' and ct.uid_employee = '$uid' and $addWhere";
            }
        } elseif (in_array($format, ['pending', 'approve', 'reject'])) {
            switch ($format) {
                case 'pending':
                    $addWhere = "ct.status is null";
                    break;
                case 'approve':
                    $addWhere = "ct.status = 'A'";
                    break;
                case 'reject':
                    $addWhere = "ct.status = 'R'";
                    break;
            }
            $uid = ($id_level == '3') ? "and ct.uid_employee = '$uid'" : '';
            $where = "ct.na = '0' and $addWhere $uid";
        }

        return $this->db->query("SELECT
                                IF
                                    (
                                        ct.`status` = 'A',
                                        'Approve',
                                    IF
                                    ( ct.`status` = 'R', 'Rejected', 'Pending' )) new_status,
                                    IF
                                    (
                                        ct.type = '1',
                                        DATE_FORMAT( ct.start_date, '%d %b %Y' ),
                                        CONCAT(
                                            DATE_FORMAT( ct.start_date, '%d %b %Y' ),
                                            ' - ',
                                        DATE_FORMAT( ct.end_date, '%d %b %Y' ))) new_date,
                                    de.full_name,
                                    ct.*,
                                    DATE_FORMAT( ct.create_date, '%d %b %Y' ) submit_date,
                                    DATE_FORMAT( ct.update_date, '%d %b %Y' ) update_date
                                FROM
                                    _data_cuti_transaksi ct
                                    LEFT JOIN _data_employee de ON ct.uid_employee = de.uid 
                                WHERE
                                    $where
                                ORDER BY
                                    ct.create_date DESC
                                    ");
    }

    public function detail_history_cuti($register)
    {
        return $this->db->query("SELECT
                                    dt.id,
                                    ct.uid_employee,
                                    ct.type,
                                    ct.qty_cuti,
                                    ct.start_date,
                                    ct.end_date,
                                    IF
                                (
                                    ct.end_date IS NULL,
                                    CONCAT(
                                    DATE_FORMAT( ct.start_date, '%d %b %y' )),
                                    CONCAT(
                                        DATE_FORMAT( ct.start_date, '%d %b %y' ),
                                        ' - ',
                                    DATE_FORMAT( ct.end_date, '%d %b %y' ))) leave_date,
                                    ct.`year`,
                                    ct.description,
                                    dt.category,
                                    ch.history,
                                IF
                                    (
                                        dt.var1 = 'A',
                                        'Approve',
                                    IF
                                    ( dt.var1 = 'R', 'Reject', NULL )) `status`,
                                    dt.note1,
                                    de.full_name,
                                    dt.create_by,
                                    dt.create_date,
                                    DATE_FORMAT( dt.create_date, '%d %b %y, %H:%i' ) new_date 
                                FROM
                                    `_data_cuti_transaksi` ct
                                    LEFT JOIN _data_history dt ON ct.register = dt.register
                                    LEFT JOIN _data_category_history ch ON dt.category = ch.id
                                    LEFT JOIN _sys_user su ON dt.create_by = su.uid
                                    LEFT JOIN _data_employee de ON su.uid = de.uid 
                                WHERE
                                    ct.register = '$register' 
                                ORDER BY
                                    dt.id DESC");
    }
}
