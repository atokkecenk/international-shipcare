<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelMenu extends CI_Model
{
    public function create_listmenu($menu, $id = null)
    {
        $id_level = $this->session->id_level;
        // $user = $this->db->get_where('_sys_user', ['username' => $this->session->username])->row();
        $params = [];
        if ($menu == 'root') {
            $params[] = "srm.id_level = '$id_level' and sl.na = '0' and su.na = '0' and srm.na = '0' and sm.id_root IS NULL";
        } elseif ($menu == 'child') {
            $params[] = "srm.id_level = '$id_level' and sl.na = '0' and sm.id_root = $id and srm.na = '0'";
        } elseif ($menu == 'route') {
            $params[] = "sm.route = '$id'";
        } else {
            $params[] = "1";
        }

        $addwhere = " where " . implode(" and ", array_filter($params));
        $sql = $this->db->query("SELECT
                    srm.id_level,
                    su.username,
                    srm.id_menu,
                    sm.`name`,
                    sm.route,
                    sm.file,
                    sm.icon,
                    sm.id_root,
                    srm.c,
                    srm.r,
                    srm.u,
                    srm.d,
                    srm.na 
                FROM
                    _sys_user su
                    LEFT JOIN _sys_role_menu srm ON su.id = srm.id_user
                    LEFT JOIN _sys_menu sm ON srm.id_menu = sm.id
                    LEFT JOIN _sys_level sl ON srm.id_level = sl.id 
                    $addwhere
                ");
        return $sql;
    }

    public function get_menu_by_route($route)
    {
        return $this->db->query("SELECT
                                    sm2.id id1,
                                    sm2.`name` name1,
                                    sm.id id2,
                                    sm.`name` name2 
                                FROM
                                    `_sys_menu` sm
                                    LEFT JOIN _sys_menu sm2 ON sm.id_root = sm2.id 
                                WHERE
                                    sm.route = '$route'
                                ")->row();
    }

    function data_login($username)
    {
        return $this->db->query("SELECT
                                    sl.id id_level,
                                    sl.level_name,
                                    su.username,
                                    de.* 
                                FROM
                                    `_sys_user` su
                                    LEFT JOIN _data_employee de ON su.uid = de.uid
                                    LEFT JOIN _sys_level sl ON su.`level` = sl.id 
                                WHERE
                                    su.username = '$username' 
                                    AND su.na = '0'");
    }
}
